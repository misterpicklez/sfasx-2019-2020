﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using System.Text.RegularExpressions;
using System.IO;

public class MenuController : MonoBehaviour
{
    //-----CLASS MEMBERS-----------------------------------------------------------------------------------------------
    //Canvas variables store references to different menus for navigating between them
    [SerializeField] private Canvas MainMenu;
    [SerializeField] private Canvas Hud;
    [SerializeField] private Canvas LevelSelectMenu;
    [SerializeField] private Canvas VictoryMenu;
    [SerializeField] private Canvas InstructionsPage;
    [SerializeField] private Canvas ConfirmationMenu;
    
    //Other necessary references for the game object and key texture for UI
    [SerializeField] private Game game;
    [SerializeField] private Texture2D keyTexture;
    
    int m_CurrentLevelPage = 1; //Holds the current page of levels being shown
    int m_NumberOfLevels = 35;  //Holds the total number of levels in the game





    //-----------------------------------------------------------------------------------------------------------------
    //-----CLASS FUNCTIONS---------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    void Start()
    {
        //Disable canvas objects that don't show on startup
        LevelSelectMenu.enabled = false;
        VictoryMenu.enabled = false;
        Hud.enabled = false;
        InstructionsPage.enabled = false;
        ConfirmationMenu.enabled = false;
    }





    //-----------------------------------------------------------------------------------------------------------------
    //-----SAVE SYSTEM FUNCTIONS---------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    //Unlocks all levels
    public void UnlockAllLevels()
    {
        Debug.Log("Cheater!");
        SaveSystem.SaveGame(1000);
    }
    public void DeleteSave()
    {
        SaveSystem.SaveGame(0);
    }

    public void OnConfirmDeleteSaveButtonPress()
    {
        DeleteSave();
        NavigateGameStates_DeleteConfirmation_to_MainMenu();
    }





    //-----------------------------------------------------------------------------------------------------------------
    //-----NAVIGATION FUNCTIONS BETWEEN GAME STATES--------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    //Navigates from 'PreviousGameState' to 'GameStateToShow'
    public void NavigateGameStates(Canvas GameStateToShow, Canvas PreviousGameState)
    {
        GameStateToShow.enabled = true;
        PreviousGameState.enabled = false;
    }          

    public void NavigateGameStates_MainMenu_to_DeleteConfirmation()
    {
        //Show confirmation menu 
        NavigateGameStates(ConfirmationMenu, MainMenu);
    }
    public void NavigateGameStates_DeleteConfirmation_to_MainMenu()
    {
        //Remove confirmation menu WITHOUT deleting save
        NavigateGameStates(MainMenu, ConfirmationMenu);
    }
    public void NavigateGameStates_MainMenu_to_Instructions()
    {
        NavigateGameStates(InstructionsPage, MainMenu);
    }
    public void NavigateGameStates_Instructions_to_MainMenu()
    {
        NavigateGameStates(MainMenu, InstructionsPage);
    }
    public void NavigateGameStates_Level_to_MainMenu()
    {
        //Show main menu, hide HUD
        NavigateGameStates(MainMenu, Hud);

        //Remove level and hide player
        game.CleanUpLevel();
    }
    public void NavigateGameStates_MainMenu_to_LevelSelect()
    {       
        //Show level select, hide main menu
        NavigateGameStates(LevelSelectMenu, MainMenu);        
    }
    public void NavigateGameStates_LevelSelect_to_MainMenu()
    {
        //Show level select, hide main menu
        NavigateGameStates(MainMenu, LevelSelectMenu);
    }
    public void NavigateGameStates_Level_to_VictoryMenu()
    {       
        //Show Victory screen on top of the level
        VictoryMenu.enabled = true;
        Hud.enabled = false;

        //If it is the last level disable the 'next level' button
        if (game.GetCurrentLevel() == m_NumberOfLevels)
        {
            VictoryMenu.transform.GetChild(0).gameObject.SetActive(false);
        }
    }
    public void NavigateGameStates_VictoryMenu_to_MainMenu()
    {
        //Show last level button again (if it were hidden)
        VictoryMenu.transform.GetChild(0).gameObject.SetActive(true);

        //Show main menu, hide victory screen
        NavigateGameStates(MainMenu, VictoryMenu);

        //Remove level and hide player
        game.CleanUpLevel();
    }
    public void NavigateGameStates_VictoryMenu_to_NextLevel()
    {
        //Show last level button again (if it were hidden)
        VictoryMenu.transform.GetChild(0).gameObject.SetActive(true);

        //Remove level and hide player
        game.CleanUpLevel();

        //Remove victory menu
        VictoryMenu.enabled = false;

        //Generate next level
        game.GenerateFromFile(game.GetCurrentLevel() + 1);
        Hud.enabled = true;

        //Reset key UI
        UpdateKeysUI(0);
    }





    //-----------------------------------------------------------------------------------------------------------------
    //-----LEVEL CONTROL FUNCTIONS-------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    //Parse buttons text to load corresponding level 
    public void OnLevelButtonPress()
    {
        //Get reference to button clicked
        GameObject levelButton = EventSystem.current.currentSelectedGameObject.gameObject;
        TextMeshProUGUI levelButtonText = levelButton.GetComponentInChildren<TextMeshProUGUI>();
        
        //\d+ is regex value for an int, use to parse for ints
        string numString = Regex.Match(levelButtonText.text, @"\d+").Value;
        int levelNum = int.Parse(numString);

        //Check if level is unlocked
        if (levelNum <= SaveSystem.LoadHighestLevel().HighestLevelCompleted + 1)
        {
            //Invoke level generation                
            Game game = GameObject.FindObjectOfType<Game>();
            if (game.GenerateFromFile(levelNum))
            {
                //Hide level select menu & show HUD
                LevelSelectMenu.enabled = false;
                Hud.enabled = true;
            }
        }
    }

    //Restart the level
    public void RestartLevel()
    {
        game.RestartLevel();
        UpdateKeysUI(0);
    }





    //-----------------------------------------------------------------------------------------------------------------
    //-----SCROLL PAGES OF LEVELS FUNCTIONS----------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    public void OnTurnButtonPress(bool direction)
    {
        //Get the amount of levels in resource folder e.g. 27
        Object[] levelList = Resources.LoadAll("Levels");       
        
        //Check direction and if it would go beyond the first or last page
        if (!direction && m_CurrentLevelPage > 1)
        {
            //Check if current page is the last page
            if (m_CurrentLevelPage >= ((float)levelList.Length / 10))
                TurnPages(direction, false, true, levelList.Length%10);
            else
                TurnPages(direction, false, false, levelList.Length % 10);
        }
        else if (direction && m_CurrentLevelPage < ( (float)levelList.Length/10))
        {
            //Check if the current page is 2nd to last i.e. the next page it turns to is last
            if (m_CurrentLevelPage >= ((float)levelList.Length / 10) - 1)
                TurnPages(direction, true, false, levelList.Length % 10);
            //else it is not last
            else
                TurnPages(direction, false, false, levelList.Length % 10);
        }
    }

    public void TurnPages(bool direction, bool goingToLastPage, bool comingFromLastPage, int numOfLevelsInLastPage)
    {
        //Get reference to level buttons parent object
        GameObject levelButtons = GameObject.Find("LevelButtons");
        //Get a list of all children i.e. level buttons
        GameObject[] levelButtonList;

        //Operate normally if not going to or coming from the last page
        if (!goingToLastPage && !comingFromLastPage)
        {
            //Find all 10 buttons
            levelButtonList = new GameObject[10];
            for (int i = 0; i < 10; i++)
            {
                levelButtonList[i] = levelButtons.transform.GetChild(i).gameObject;
            }

            //Change the numbers according to arrow pressed
            ChangeLevelNumbers(direction, levelButtonList);
        }

        //Operate accordingly if showing the last page
        else if (goingToLastPage)
        {
            //Get all buttons up to the last level
            levelButtonList = new GameObject[numOfLevelsInLastPage];
            for (int i = 0; i < numOfLevelsInLastPage; i++)
            {
                levelButtonList[i] = levelButtons.transform.GetChild(i).gameObject;
            }

            //Change the numbers according to arrow pressed
            ChangeLevelNumbers(direction, levelButtonList);

            //Disable trailing children if numOfLevelsInLastPage
            for (int i = 9; i >= numOfLevelsInLastPage; i--)
            {
                levelButtons.transform.GetChild(i).gameObject.SetActive(false);                              
            }            
        }   
        //Operate accordingly if coming from the last page
        else
        {
            //Get all buttons up to the last level
            levelButtonList = new GameObject[numOfLevelsInLastPage];
            for (int i = 0; i < numOfLevelsInLastPage; i++)
            {
                levelButtonList[i] = levelButtons.transform.GetChild(i).gameObject;
            }

            //Change the numbers according to arrow pressed
            ChangeLevelNumbers(direction, levelButtonList);

            //Reenable trailing children
            for (int i = 9; i >= numOfLevelsInLastPage; i--)
            {
                levelButtons.transform.GetChild(i).gameObject.SetActive(true);
            }
        }        

        //Update page counter
        if (direction)
            m_CurrentLevelPage++;
        else
            m_CurrentLevelPage--;
    }

    public void ChangeLevelNumbers(bool direction, GameObject[] levelButtonList)
    {
        //Loop through level buttons
        foreach (GameObject button in levelButtonList)
        {
            //Get the number in the button                
            string numString = Regex.Match(button.transform.GetComponentInChildren<TextMeshProUGUI>().text, @"\d+").Value;
            int levelNum = int.Parse(numString);

            //Add or subtract 10 depending on direction and put new number back in button
            if (direction)
                button.transform.GetComponentInChildren<TextMeshProUGUI>().text = "Level " + (levelNum + 10);
            else
                button.transform.GetComponentInChildren<TextMeshProUGUI>().text = "Level " + (levelNum - 10);
        }
    }





    //-----------------------------------------------------------------------------------------------------------------
    //-----HUD UPDATE FUNCTIONS----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    public void UpdateKeysUI(int numOfKeys)
    {
        //Clear all previous keys
        GameObject[] KeySprites = GameObject.FindGameObjectsWithTag("KeySprite");
        foreach (GameObject sprite in KeySprites)                    
            Destroy(sprite);            
        

        for (int i = 0; i < numOfKeys; i++)
        {
            //Create new gameobject 
            GameObject keySprite = new GameObject("Key Sprite");
            keySprite.tag = "KeySprite";

            //Attach sprite component to it
            keySprite.AddComponent<SpriteRenderer>();
            keySprite.GetComponent<SpriteRenderer>().sprite = Sprite.Create(keyTexture, new Rect(0.0f, 0.0f, keyTexture.width, keyTexture.height), new Vector3(0.0f, 0.0f), 100.0f);

            //Attach gameobject to HUD
            keySprite.transform.parent = Hud.transform;
            keySprite.transform.localPosition = Vector3.zero;

            //Set position of sprite
            keySprite.AddComponent<RectTransform>();
            keySprite.GetComponent<RectTransform>().anchorMin = new Vector2(0, 1);
            keySprite.GetComponent<RectTransform>().anchorMax = new Vector2(0, 1);
            keySprite.transform.rotation = new Quaternion(0, 0, 0, 0);
            keySprite.transform.localPosition += new Vector3(50 + (150 * i), -75, 0); //.Set(50 +(keyTexture.width * i), -75, 0);                                         
        }
    }
}
