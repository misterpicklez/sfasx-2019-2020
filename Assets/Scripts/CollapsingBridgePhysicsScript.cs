﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollapsingBridgePhysicsScript : MonoBehaviour
{
    [SerializeField] GameObject[] m_Planks = new GameObject[5];
    private Vector3[] m_PlankDownVelocities = new Vector3[5];
    private Vector3[] m_PlankDownForcePositions = new Vector3[5];

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < m_Planks.Length; i++)
        {
            float downForce = Random.Range(2500, 5000);
            Vector3 downVector = new Vector3(0, -downForce, 0);

            m_PlankDownVelocities[i] = downVector;


            float xpos = Random.Range(-10, 10);
            m_PlankDownForcePositions[i] = new Vector3(xpos, m_Planks[i].transform.position.y, m_Planks[i].transform.position.z);
        }

        //Expire after a few seconds
        StartCoroutine(ExpireAfterTime());
    }

    private void Update()
    {
        //Apply downward forces to the planks to make them fall fast
        for(int i = 0; i < m_Planks.Length; i++)
        {
            m_Planks[i].GetComponent<Rigidbody>().AddForceAtPosition(m_PlankDownVelocities[i], m_PlankDownForcePositions[i]);
        }
    }


    IEnumerator ExpireAfterTime()
    {
        yield return new WaitForSeconds(2);
        Destroy(gameObject);
    }
}
