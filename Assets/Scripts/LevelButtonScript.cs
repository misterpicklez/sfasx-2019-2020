﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine.UI;

public class LevelButtonScript : MonoBehaviour
{  
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        string numString = Regex.Match(gameObject.transform.GetComponentInChildren<TextMeshProUGUI>().text, @"\d+").Value;
        int levelNum = int.Parse(numString);

        //If level number is higher than the previous highest level completed
        if (levelNum > SaveSystem.LoadHighestLevel().HighestLevelCompleted + 1)
        {
            //Change button to display padlock icon
            var padlockSprite = Resources.Load<Sprite>("Sprites/LockedLevel");
            gameObject.GetComponent<Image>().sprite = padlockSprite;       
        }

        if (levelNum <= SaveSystem.LoadHighestLevel().HighestLevelCompleted + 1)
        {
            //Remove button image
            gameObject.GetComponent<Image>().sprite = null;
        }
    }
}
