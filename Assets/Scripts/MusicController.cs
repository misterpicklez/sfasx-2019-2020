﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class MusicController : MonoBehaviour
{
    public AudioSource m_Music;
    public Sprite m_MusicOnSprite;
    public Sprite m_MusicOffSprite;
    private bool m_MusicOn = true;
 
    
    public void ToggleMusic()
    {
        //Flip boolean
        m_MusicOn = !m_MusicOn;

        //Play/stop based on boolean toggle
        if (m_MusicOn)
        {            
            m_Music.Play();

            //Also change mute button sprite
            gameObject.GetComponent<Image>().sprite = m_MusicOnSprite;

        }
        else
        {
            m_Music.Stop();

            gameObject.GetComponent<Image>().sprite = m_MusicOffSprite;
        }               
    }    
}
