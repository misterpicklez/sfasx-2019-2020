﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChristmasTreeScript : MonoBehaviour
{
    [SerializeField] public GameObject m_Present;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SpawnPresent()
    {
        GameObject present = Instantiate(m_Present, gameObject.transform);
        present.transform.localPosition = new Vector3(8.54f, 2.5f, 2.58f);
        present.transform.localScale = new Vector3(7.5f, 7.5f, 7.5f);
    }
}
