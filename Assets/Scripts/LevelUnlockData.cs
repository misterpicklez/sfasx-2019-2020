﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable] //Means we can save it in a file
public class LevelUnlockData
{
    public int HighestLevelCompleted = 0;

    //Constructor
    public LevelUnlockData(int highestLevel)
    {
        HighestLevelCompleted = highestLevel;
    }

    //Set new highest level
    public void SetHighestLevelCompleted(int level)
    {
        HighestLevelCompleted = level;
    }
}
