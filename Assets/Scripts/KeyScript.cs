﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyScript : MonoBehaviour
{
    bool m_Up = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Rotate the key
        this.transform.Rotate(0, 60 * Time.deltaTime, 0, Space.World);

        if (transform.position.y <= 4 || transform.position.y >= 7)
            m_Up = !m_Up;

        if (m_Up)
            transform.Translate(0, 2.5f * Time.deltaTime, 0, Space.World);
        else
            transform.Translate(0, -2.5f * Time.deltaTime, 0, Space.World);
    }

    //Remove object when picked up
    public void PickUpKey()
    {
        Destroy(this.gameObject);
    }
}
