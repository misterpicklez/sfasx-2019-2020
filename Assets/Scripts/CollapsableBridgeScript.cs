﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CollapsableBridgeScript : MonoBehaviour
{
    [SerializeField] public GameObject m_DestroyedVersion;
    [SerializeField] public GameObject m_EmptySpace;
    // Start is called before the first frame update

    private void Start()
    {
        transform.localPosition -= new Vector3(0, 10, 0);
        //Wait for collapse animation to play
        StartCoroutine(CollapseAfterTime());

        //Swap this animated object with falling physics object
        

    }

    IEnumerator CollapseAfterTime()
    {        
        yield return new WaitForSeconds(0.75f);
        //Instantiate a collapsing bridge model
        GameObject FallingPieces = Instantiate(m_DestroyedVersion);
        //Set the new block as a child of the plane
        FallingPieces.transform.SetParent(gameObject.transform.parent);
        FallingPieces.transform.localPosition = new Vector3(5, -5, 5);


        //Change this plane to be an empty plane
        gameObject.transform.parent.gameObject.tag = "Untagged";

        //Destroy this
        Destroy(gameObject);        
    }
}
