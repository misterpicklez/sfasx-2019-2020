﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    //-----CLASS MEMBERS-----------------------------------------------------------------------------------------------
    enum E_Direction
    {
        up, 
        down,
        left,
        right
    }

    //Public-----
    public Rigidbody m_Rb;
    public int m_Keys = 0;                          //Amount of keys the player has
    public GameObject m_TrailsParticleSystem;       //Reference to particle system
    public GameObject m_BurstParticleSystem;        //Reference to cloud burst particle system
    //Private----
    [SerializeField] private float SingleNodeMoveTime = 0.5f;
    private MenuController m_MenuController;        //Reference menu navigation scripts
    private Game m_Game;                            //Reference for game
    private Vector3 m_Velocity = Vector3.zero;      //Velocity
    private float   m_InitialPush = 3.5f;//10;           //Initial push when moving
    private float   m_Acceleration = 1.045f;          //Acceleration
    private float   m_MaxSpeed = 60.0f;             //Max speed
    private bool    m_BlockInput = false;           //Determine if player can give a new move input
    

    //Variables for moving to center of tile...
    private float   m_startTime;                    //Start time for lerp function
    private float   m_distToCenter;                 //Distance to cover until center of tile for lerp function
    private Vector3 m_CurrentCoord;                 //Current coord for lerp function
    private Vector3 m_CenterCoord;                  //Center of tile coord for lerp function
    private float   m_ResetSpeed = 12.0f;           //Speed of lerp function movement
    private bool    m_MovingToCenter = false;       //Check for if currently moving towards center
    private EnvironmentTile m_LastTileTouched;

    //Variables for checking if player has fallen off the level    
    private Vector3 m_FallingVector = new Vector3(0, -50, 0);   //Vector to move player if they're falling
    private int     m_PlayerOnFloor = 0;                        //If this falls below 0 the player is not on any floor

    //Variables for breakable block mechanic
    private int     m_Momentum = 0;             //Stores how many spaces the player has moved
    private bool    m_HasBrokeBlock = false;    //Stores whether player can gain more momentum or not
    private bool    m_HasMoved = false;         //Stores whether player has moved ever, to prevent spawning giving momentum

    //-----GETTERS & SETTERS------------------------------------------------------------------------------------------
    public EnvironmentTile CurrentPosition { get; set; }





    //-----------------------------------------------------------------------------------------------------------------
    //-----CLASS FUNCTIONS---------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------

    /// <summary>
    /// Setup necessary object references & initial player state
    /// </summary>
    private void Start()
    {
        m_MenuController = GameObject.FindObjectOfType<MenuController>();
        m_Game = GameObject.FindObjectOfType<Game>();
        m_LastTileTouched = FindObjectOfType<Environment>().Start;
        m_Momentum = 0;
    }

    /// <summary>
    /// Updates player
    /// </summary>
    private void Update()
    {
        //Once player has fallen to a certain height, restart the level
        if (m_Rb.position.y <= -50)
            m_MenuController.RestartLevel();
        //Check if player is falling
        if (m_PlayerOnFloor <= 0)                
            m_Rb.MovePosition(m_Rb.position += m_FallingVector * Time.deltaTime);            
        
        //Else if moving toward center of tile
        else if (m_MovingToCenter)
        {
            MoveToCenter();
        }        
        //Else if player is in movement state
        else if (m_Velocity != Vector3.zero)
        {
            MovePlayer();                        
        }
        //If player is not currently in movement state
        else
        {
            //Check if player is still on an empty space (by colliding with an obstacle on a collapsing floor)
            Physics.Raycast(m_Rb.position + Vector3.up, Vector3.down * 100, out RaycastHit hit, 100);
            if (hit.collider != null)
            {
                if (hit.collider.gameObject.tag == "Untagged")                                  
                    m_PlayerOnFloor--;
                
            }

            //Check if player input is enabled
            if (!m_BlockInput && m_LastTileTouched.tag != "CollapsingBridge")
            {
                if (Input.GetKeyDown(KeyCode.LeftArrow))    //Left
                {
                    m_Rb.transform.eulerAngles = new Vector3(0, 270f, 0);   //Rotate character
                    Move(E_Direction.left);                                 //Move in direction
                }
                if (Input.GetKeyDown(KeyCode.RightArrow))   //Right
                {
                    m_Rb.transform.eulerAngles = new Vector3(0, 90.0f, 0);
                    Move(E_Direction.right);
                }
                if (Input.GetKeyDown(KeyCode.DownArrow))    //Down
                {
                    m_Rb.transform.eulerAngles = new Vector3(0, 180, 0);
                    Move(E_Direction.down);
                }
                if (Input.GetKeyDown(KeyCode.UpArrow))      //Up
                {
                    m_Rb.transform.eulerAngles = new Vector3(0, 0, 0);
                    Move(E_Direction.up);
                }
            }            
        }       
        

    }

    /// <summary>
    /// Move player in direction provided until they hit an obstacle 
    /// </summary>
    /// <param name="pdirection"></param>
    private void Move(E_Direction pdirection)
    {
        m_HasMoved = true;

        //Block further user input
        m_BlockInput = true;
                
        //Move player in direction
        switch(pdirection)
        {
            case E_Direction.up:
                {
                    //m_Rb.AddForce(0, 0, m_InitialPush * Time.deltaTime);
                    m_Velocity = new Vector3(0, 0, m_InitialPush);
                    break;
                }
            case E_Direction.down:
                {
                    //m_Rb.AddForce(0, 0, -m_InitialPush * Time.deltaTime);
                    m_Velocity = new Vector3(0, 0, -m_InitialPush);
                    break;
                }
            case E_Direction.left:
                {
                    //m_Rb.AddForce(-m_InitialPush * Time.deltaTime, 0, 0);
                    m_Velocity = new Vector3(-m_InitialPush, 0, 0);
                    break;
                }
            case E_Direction.right:
                {
                    //m_Rb.AddForce(m_initialPush * Time.deltaTime, 0, 0);
                    m_Velocity = new Vector3(m_InitialPush, 0, 0);
                    break;
                }
            default:
                {
                    break;
                }
        }
        
    }




    
    //-----------------------------------------------------------------------------------------------------------------
    //-----COLLISION DETECTION-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------

    /// <summary>
    /// Check if player has collided with an object
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter (Collision collision)
    {
        //Check for win--------------------------------------------------------------
        if (collision.gameObject.tag == "Finish")
        {
            collision.gameObject.GetComponent<ChristmasTreeScript>().SpawnPresent();
            Win();
        }

        //Check for pickups----------------------------------------------------------
        if (collision.gameObject.tag == "Key")
        {            
            collision.gameObject.GetComponent<KeyScript>().PickUpKey();
            AddKey();
        }

        //Check for interactable obstacles-------------------------------------------
        if (collision.gameObject.tag == "Lock")
        {
            //Check if player has a key
            if (m_Keys > 0)
            {
                //Change tile tag to let player move through
                collision.gameObject.tag = "Floor";
                //Remove the block child object
                Destroy(collision.gameObject.transform.GetChild(0).gameObject);
                //Remove one key from player
                RemoveKey();

            }            
            else //block player
                CollideWithObstacle();
        }
        if (collision.gameObject.tag == "Breakable")
        {
            //Check if player has enough momentum
            if (m_Momentum >= 4)
            {
                m_HasBrokeBlock = true;
                RemoveMomentum();
                collision.gameObject.GetComponent<BreakableBlockScript>().OnBreak();
            }
            else //block player
                CollideWithObstacle();
        }
        if (collision.gameObject.tag == "Bridge")          
            collision.gameObject.GetComponent<BreakableBlockScript>().OnBreak();
        

        //Finally check if tile is floor or obstacle----------------------------------
        if (collision.gameObject.tag == "Floor" || collision.gameObject.tag == "Bridge" || collision.gameObject.tag == "CollapsingBridge")
        {
            AddMomentum();            
            m_PlayerOnFloor++;
            m_LastTileTouched = collision.gameObject.GetComponent<EnvironmentTile>();
        }
        else if (collision.gameObject.tag == "Obstacle")
        {
            CollideWithObstacle();
        }                   
    }

    /// <summary>
    /// Check if player has fallen off map
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionExit(Collision collision)
    {    
        if (collision.gameObject.tag == "Floor" || collision.gameObject.tag == "CollapsingBridge")                  
            m_PlayerOnFloor--;                    
    }





    //-----------------------------------------------------------------------------------------------------------------
    //-----HELPER FUNCTIONS--------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    //Add momentum to player every time they move into a new block
    private void AddMomentum()
    {
        //Check not at maximum momentum && hasn't broken a block this move
        if (m_Momentum < 5 && !m_HasBrokeBlock && m_HasMoved)
        {
            m_Momentum++;
            //Play particle system if players momentum goes above 3
            if (m_Momentum >= 4)
            {                
                m_TrailsParticleSystem.GetComponent<ParticleSystem>().Play();
                if (m_Momentum == 5)
                    m_BurstParticleSystem.GetComponent<ParticleSystem>().Play();
            }
        }        
        
            
        //if (m_Momentum >= 5)
        //{
        //    var trails = m_ParticleSystem.GetComponent<ParticleSystem>().trails;
        //    trails.enabled = true;
        //}
    }

    //Remove momentum from player every time they break a block
    private void RemoveMomentum()
    {
        m_Momentum -= 1;

        //Stop particle system if player momentum goes below 4
        if (m_Momentum <= 3)
            m_TrailsParticleSystem.GetComponent<ParticleSystem>().Stop();
    }

    //Set player momentum to 0 when they hit an obstacle
    private void SetMomentumZero()
    {
        m_Momentum = 0;
    }

    //Stop the player from moving and reset them to the center of their current plane
    private void CollideWithObstacle()
    {
        //Stop all forces on the player 
        m_Velocity = Vector3.zero;
        //Remove particle effects
        m_TrailsParticleSystem.GetComponent<ParticleSystem>().Stop();
        
        //Move them into the center of the current tile
        m_MovingToCenter = true;
        m_CenterCoord = m_LastTileTouched.transform.position + new Vector3(5, 2.5f, 5);

        //If player is on a collapsing bridge fix center coordinate (because the model is no centered)
        if (m_LastTileTouched.tag == "CollapsingBridge")        
            m_CenterCoord += new Vector3(-5, 0, -5);
        
            
        m_CurrentCoord = m_Rb.transform.position;
        m_distToCenter = Vector3.Distance(m_Rb.position, m_CenterCoord);
        m_startTime = Time.time;           
    }

    //Moves the player to the center of their current plane
    private void MoveToCenter()
    {
        //Distance moved equals elapsed time * speed...
        float distCovered = (Time.time - m_startTime) * m_ResetSpeed;

        //Fraction of journey completed equals current distance divided by total distance
        float fractionOfJourney = distCovered / m_distToCenter;

        if (fractionOfJourney > 1)
            fractionOfJourney = 1; //Prevent lerp function from overshooting

        //Set position as fraction of distance
        m_Rb.position = Vector3.Lerp(m_CurrentCoord, m_CenterCoord, fractionOfJourney);

        if (m_Rb.position == m_CenterCoord)
        {
            SetMomentumZero();          //Set momentum to 0 again
            m_HasBrokeBlock = false;    //Let player gain momentum again
            m_BlockInput = false;       //Resume letting player give inputs
            m_MovingToCenter = false;   //Exit scope
        }
    }

    //Moves player based on their velocity
    private void MovePlayer()
    {
        //Check player won't overstep into the next tile (and fall through the floor)          
        Physics.Raycast(m_Rb.position + Vector3.up, (m_Velocity * Time.deltaTime), out RaycastHit hit, (m_Velocity * Time.deltaTime).magnitude);
        if (hit.collider != null)
        {
            switch (hit.collider.gameObject.tag)
            {
                case "Obstacle":
                    CollideWithObstacle();
                    break;
                case "Victory":
                    CollideWithObstacle();
                    break;
                case "Lock":
                    if (m_Keys <= 0)
                        CollideWithObstacle();
                    break;
                case "Breakable":
                    if (m_Momentum < 4)
                        CollideWithObstacle();
                    break;
                default:
                    break;
            }                        
        }


        //Move with velocity 
        m_Rb.transform.position += (m_Velocity * Time.deltaTime);
        //m_Rb.MovePosition(m_Rb.position + (m_Velocity * Time.deltaTime));
        //Accelerate until at max speed
        if (Mathf.Abs(m_Velocity.x) < m_MaxSpeed && Mathf.Abs(m_Velocity.z) < m_MaxSpeed)
            m_Velocity *= m_Acceleration;
        
    }

    private void AddKey()
    {
        m_Keys++;
        m_MenuController.UpdateKeysUI(m_Keys);
    }

    private void RemoveKey()
    {
        m_Keys--;
        m_MenuController.UpdateKeysUI(m_Keys);
    }

    private void Win()
    {
        //If highest level achieved, update save file       
        m_Game.UpdateSaveFile();
        m_Velocity = new Vector3(0, 0, 0);
        m_BlockInput = true;
        m_MenuController.NavigateGameStates_Level_to_VictoryMenu();
    }





    //-----------------------------------------------------------------------------------------------------------------
    //-----DEPRECATED FUNCTIONS----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    //Deprecated, move individual tile
    private IEnumerator DoMove(Vector3 position, Vector3 destination)
    {
        // Move between the two specified positions over the specified amount of time
        if (position != destination)
        {
            transform.rotation = Quaternion.LookRotation(destination - position, Vector3.up);

            Vector3 p = transform.position;
            float t = 0.0f;

            while (t < SingleNodeMoveTime)
            {
                t += Time.deltaTime;
                p = Vector3.Lerp(position, destination, t / SingleNodeMoveTime);
                transform.position = p;
                yield return null;
            }
        }
    }

    //Deprecated, loop movement through tiles in a given route
    private IEnumerator DoGoTo(List<EnvironmentTile> route)
    {
        // Move through each tile in the given route
        if (route != null)
        {
            Vector3 position = CurrentPosition.Position;
            for (int count = 0; count < route.Count; ++count)
            {
                Vector3 next = route[count].Position;
                yield return DoMove(position, next);
                CurrentPosition = route[count];
                position = next;
            }
        }
    }

    //Deprecated, base project version of clicking a position to move via auto pathing
    public void GoTo(List<EnvironmentTile> route)
    {
        // Clear all coroutines before starting the new route so 
        // that clicks can interupt any current route animation
        StopAllCoroutines();
        StartCoroutine(DoGoTo(route));
    }

}
