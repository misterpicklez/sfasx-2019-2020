﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrokenBlockScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ClearFragments());               
    }

    IEnumerator ClearFragments()
    {
        //Get the children (fragments)
        Transform[] children = gameObject.GetComponentsInChildren<Transform>();

        //Clear the sphere that was placed to create the explosion effect
        Destroy(children[children.Length - 1].gameObject);

        //Wait for fraction of a second
        yield return new WaitForSecondsRealtime(0.25f);        

        //Delete fragments in sets of 3 with a delay between each set
        for (int i = 1; i < children.Length - 1; i++)
        {                                    
            Destroy(children[i].gameObject);
            if (i % 3 == 0)
                yield return new WaitForSecondsRealtime(0.1f);            
        }
    }    
}
