﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class InstructionsPageScript : MonoBehaviour
{
    [SerializeField] GameObject[] m_Pages;    
    private int m_CurrentPage = 0;

    private void Start()
    {
        foreach (GameObject page in m_Pages)
        {
            page.SetActive(false);
        }
        m_Pages[0].SetActive(true);
        m_Pages[0].GetComponentInChildren<VideoPlayer>().Play();
    }

    public void NextPage()
    {
        if (m_CurrentPage < m_Pages.Length -1)
        {
            //Reset video to start 
            m_Pages[m_CurrentPage].GetComponentInChildren<VideoPlayer>().Stop();

            //Turn page
            m_Pages[m_CurrentPage].SetActive(false);
            m_CurrentPage++;
            m_Pages[m_CurrentPage].SetActive(true);

            //Play video            
            m_Pages[m_CurrentPage].GetComponentInChildren<VideoPlayer>().Play();
        }
    }
    public void PreviousPage()
    {
        if (m_CurrentPage > 0)
        {
            //Reset video to start 
            m_Pages[m_CurrentPage].GetComponentInChildren<VideoPlayer>().Stop();

            //Go back a page
            m_Pages[m_CurrentPage].SetActive(false);
            m_CurrentPage--;
            m_Pages[m_CurrentPage].SetActive(true);

            //Play video            
            m_Pages[m_CurrentPage].GetComponentInChildren<VideoPlayer>().Play();
        }
    }
}
