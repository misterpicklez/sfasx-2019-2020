﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakableBlockScript : MonoBehaviour
{
    public GameObject DestroyedVersion;
    public GameObject ObjectToDelete;
    public void OnBreak()
    {
        //Change the plane tag to floor (allows player to move on it)
        gameObject.tag = "Floor";
        //Change plane tag to special type of collapsing floor if applicable
        if (ObjectToDelete.tag == "Bridge")            
            gameObject.tag = "CollapsingBridge";
        //Instantiate the broken block
        GameObject destroyed = Instantiate(DestroyedVersion, transform.position, transform.rotation);
        //Set the new block as a child of the plane
        destroyed.transform.SetParent(gameObject.transform);
        destroyed.transform.localPosition = new Vector3(5, 2.5f, 5);
        //Destroy the block (don't change the plane i.e. the parent obj)
        Destroy(ObjectToDelete);
        
    }

    
}
