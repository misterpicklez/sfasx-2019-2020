﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    //Saves to a binary file the highest level that has been completed
     public static void SaveGame(int highestLevel)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Path.Combine(Application.persistentDataPath, "SaveState.bi");
        FileStream stream = new FileStream(path, FileMode.Create);

        LevelUnlockData data = new LevelUnlockData(highestLevel);
        formatter.Serialize(stream, data);
        stream.Close();
    }

    public static LevelUnlockData LoadHighestLevel()
    {
        string path = Path.Combine(Application.persistentDataPath, "SaveState.bi");
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            LevelUnlockData data = formatter.Deserialize(stream) as LevelUnlockData;
            stream.Close();
            return data;
        }
        else
        {
            Debug.LogError("Save file not found in " + path);
            return null;
        }
    }


    public static void InitialiseSaveFile()
    {
        //Check if save file already exists, if not create one
        string path = Path.Combine(Application.persistentDataPath, "SaveState.bi");
        if (!File.Exists(path))        
            SaveGame(0);
    }
}
