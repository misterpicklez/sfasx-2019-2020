﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Game : MonoBehaviour
{
    [SerializeField] private Camera MainCamera;
    [SerializeField] private Character Character;
    [SerializeField] private Canvas Menu;
    [SerializeField] private Canvas Hud;
    [SerializeField] private Transform CharacterStart;
    [SerializeField] private MenuController menuController;

    private int m_CurrentLevel;
    public int GetCurrentLevel() { return m_CurrentLevel; }

    private RaycastHit[] mRaycastHits;
    private Character mCharacter;
    private Environment mMap;

    private readonly int NumberOfRaycastHits = 1;

    //-----CLASS FUNCTIONS--------------------------------------------------------------------------------------------
    //Public---------------------------------    
    public void CleanUpLevel()
    {
        //Remove player
        Destroy(mCharacter.gameObject);               
        mMap.CleanUpWorld();
    }
    public void SetPlayerToStartPos()
    {
        mCharacter.transform.position = mMap.Start.Position;
        mCharacter.transform.rotation = Quaternion.identity;
        mCharacter.CurrentPosition = mMap.Start;
    }
    public void HidePlayer(){ mCharacter.enabled = false; }
    public void ShowPlayer(){ mCharacter.enabled = true; }


    public void Generate()
    {
        mCharacter = Instantiate(Character, transform);
        mMap.GenerateWorld();
    }

    public void RestartLevel()
    {
        CleanUpLevel();        
        GenerateFromFile(m_CurrentLevel);
    }

    public bool GenerateFromFile(int levelNum)
    {
        if (Resources.Load<TextAsset>("Levels/level" + levelNum) != null)
        {
            m_CurrentLevel = levelNum;
            mCharacter = Instantiate(Character, transform);
            mMap.GenerateFromFile(levelNum);

            //Place character
            mCharacter.transform.position = mMap.Start.Position;
            mCharacter.transform.rotation = Quaternion.identity;
            mCharacter.CurrentPosition = mMap.Start;
            return true;
        }
        Debug.Log("Error: level tilemap file does not exist");
        return false;
    }

    public void UpdateSaveFile()
    {
        if (SaveSystem.LoadHighestLevel().HighestLevelCompleted < m_CurrentLevel)
            SaveSystem.SaveGame(m_CurrentLevel);
    }
    public void Exit()
    {
#if !UNITY_EDITOR
        Application.Quit();
#endif
    }

    //Private---------------------------------
    private void Start()
    {
        mRaycastHits = new RaycastHit[NumberOfRaycastHits];
        mMap = GetComponentInChildren<Environment>();        
    }

    private void Awake()
    {
        SaveSystem.InitialiseSaveFile();
    }

    private void Update()
    {
        // Check to see if the player has clicked a tile and if they have, try to find a path to that 
        // tile. If we find a path then the character will move along it to the clicked tile. 
        //if (Input.GetMouseButtonDown(0))
        //{
        //    Ray screenClick = MainCamera.ScreenPointToRay(Input.mousePosition);
        //    int hits = Physics.RaycastNonAlloc(screenClick, mRaycastHits);
        //    if (hits > 0)
        //    {
        //        EnvironmentTile tile = mRaycastHits[0].transform.GetComponent<EnvironmentTile>();
        //        if (tile != null)
        //        {
        //            List<EnvironmentTile> route = mMap.Solve(mCharacter.CurrentPosition, tile);
        //            mCharacter.GoTo(route);
        //        }
        //    }
        //}
    }

    


    
}
